	"use strict";
	 
	
	var jq = jQuery.noConflict();
	
	jq(function(){
	setup();
	});
	
	
	var canvas = {
		createCanvas: function(dimensions,position,zoom){
			this._position = position || {x:40, y:50};
			this._zoom = zoom || 1;
			this._dimensions = dimensions || {width:3000, height:3000};
			this._self = this;
			this._domElement = jq('<div>');
			this._containerDomElement = jq('#viewport');
                        
			var parentElement = jq(this._appendToID);
			
			var jqElement = jq(this._domElement);
			jqElement.attr('id', 'canvas');
			//jqElement.css({'top' :this._position.y+'px','left':this._position.x+'px'});
						
			jqElement.appendTo(parentElement); //jqElement is appended to the object whose ID is specified in _appendToID
			
                        
			var container = this._containerDomElement;
			
			this._container.width = container.width(); //does this work??? !!!
			this._container.height = container.height();

			this._offset = container.offset();
			this._mouseStartPos ={x:0,y:0}; //contains startposition of the mouse (the "down" event) 
			
			//bind the "this" to the object's Event Handlers !!! overwritesFunctions
			this.downHandler = this.downHandler.bind(this);
			this.moveHandler = this.moveHandler.bind(this);
			this.upHandler = this.upHandler.bind(this);
			this.zoomHandler = this.zoomHandler.bind(this);
			
			container.on('mousewheel', this.zoomHandler); //was: jq(this._domElement).on('mousewheel', this.zoomHandler);
			container.on('mousedown', this.downHandler);  //was: jq(this._domElement).on('mousedown', this.downHandler);
                        
			jqElement.css({
				'transform':'translate('+
					this._position.x+
					'px'+
					','+
					this._position.y+
					'px'+
					')',
				'width':this._dimensions.width,
				'height':this._dimensions.height,
				'top':0,
				'position':'absolute',
				'transform-origin':'0 0'
			});
			
			return this._domElement;
		},
		downHandler:function(event){

			if(event.button!==2){
				return
			}
			this._mouseStartPos = { //is part of the object, so the differences to it can be calculated elsewhere
				'x':event.pageX,
				'y':event.pageY
			};

			this._objectStartPos = {
				x:this._position.x,
				y:this._position.y
			};
                        
			this._containerDomElement.on('mousemove',this.moveHandler);
		},
		moveHandler:function(event){
			event.preventDefault();

			var offset={ //contains Difference between startposition (click) and current mouse position
				'x': event.pageX - this._mouseStartPos.x,
				'y': event.pageY - this._mouseStartPos.y
			};

			//FIXIT: 
			/*Should later take jut offset not additional old stuff, which could be more efficiently retrieved from a dedicated "canvas" object */

			var newPos = this._constrain({x:this._objectStartPos.x+offset.x,y:this._objectStartPos.y+offset.y}); 
			this._setXYS(this._domElement,newPos);
			//console.log(oldObjectPos, '|', offset.x,' ',offset.y);
		},

		upHandler:function(event){
			                        
                        jq(this._containerDomElement).off('mousemove',this._moveHandler);
                        jq('#view').off('mouseleave', this._upHandler); 
		},

		zoomHandler:function(event, delta){//could trigger a zoom event to trigger actions regarding zooming
					//input: domObject to be scaled, the delta, event object (a mousewheel usually)

					//BEDENKE: Pixel-Werte für translate sind relativ zu der außen-Box angegeben, 
					//sie verändern sich nicht bei unterschiedlichen scale-Werten

					//Delta to Zoom factor
					event.preventDefault();
					
					if(delta<0){ 	//delta is -1.3... or +1.3... depending on scroll direction. 
									//Zoom values should be in %, so that 0 is infinite small and
									//200 is double the size. So delta is recalculated to be positive, but smaller 
									//if it is -1.3...
							delta = 1/Math.abs(delta);
					}


					var posX = this._position.x;
					var posY = this._position.y;

					var oldScale = this._zoom;
					var newScale = oldScale*delta;
					var scaleFactor = newScale/oldScale;

					var unit="px";

					var offset_canvas = {
							'x':this._position.x,
							'y':this._position.y
					};

					var offset_view = {
						'x': this._offset.left,
						'y': this._offset.top
					}

					//Berechne Position des Zoom-Centers (das Zoom-Center)
					var scaleOrigin = {
							'x':	offset_view.x+ //verschiebung des "Viewports"
											//(jq(domObject).outerWidth() / 2)+ //Mitte der Canvas
											offset_canvas.x, //verschiebung Canvas

							'y':	offset_view.y+ //verschiebung des "Viewports"
											//(jq(domObject).outerHeight() / 2)+ //Mitte der Canvas
											offset_canvas.y //verschiebung Canvas
					};

					//berechne die Mausposition relativ zum skalierungsursprung.
					var scaleOriginMouse_offset = {
							'x':event.pageX-
									scaleOrigin.x,

							'y':event.pageY-
									scaleOrigin.y
					}

					var newX = offset_canvas.x+scaleOriginMouse_offset.x*(1-scaleFactor); 
					var newY = offset_canvas.y+scaleOriginMouse_offset.y*(1-scaleFactor);

					var sanitizedValues = this._constrain({x:newX,y:newY,s:newScale}); //<-- how to solve this?

//to setxys
					this._setXYS(this._domElement, sanitizedValues);
				},
		_constrain:function(xysObject){
                                        
					var newXysObject=xysObject;

                                        //sanitize extreme zoom settings if needed: 
                                        if(xysObject.s &&(xysObject.s>10||xysObject.s<0.1)){ 
							newXysObject.s = this._zoom;
                                                        newXysObject.x = this._position.x;
                                                        newXysObject.y = this._positiony;
						}
                                     
                                        return newXysObject;
                                        
                                        
		},
		
		_setXYS:function(domObject,xysObject){
			
			var unit="px";
			
			if(!domObject){
			console.log("ERROR: no Dom-Object passed to getXYfromTransform");
			}
			
			if(!xysObject){
			console.log("ERROR: no XYZ-Object passed to getXYfromTransform");
			}
			
			
			if (!xysObject.x){
				xysObject.x = Math.round(this._position.x)
			}else{
				xysObject.x = Math.round(xysObject.x)
			}
			
			if (!xysObject.y){
				xysObject.y = Math.round(this._position.y)
			}else{
				xysObject.y = Math.round(xysObject.y)
			}
			
			if (!xysObject.s){xysObject.s = this._zoom}	
			
			
				
			jq(domObject).css({"transform":"translate("+
				xysObject.x+
				unit+
				","+
				xysObject.y+
				unit+
				")"+
				"scale("+
				xysObject.s+
				")"	
				});
			this._position.x = xysObject.x;
			this._position.y = xysObject.y;
			this._zoom = xysObject.s;
			
			console.log("moved X:"+xysObject.x+"|Y:"+xysObject.y+" | s: "+xysObject.s);
			return xysObject;
		},
		_domElement:{},
                _containerDomElement:undefined,
		_container:{
			width:undefined,
			height:undefined
		},
		_offset:{x:0,y:0}, //offset relative to document
		_position:{x:0, y:0},
		_dimensions:{width:1000, height:1000},
		_zoom:1,//absolute zooming value
		_appendToID:'#viewport',
		_mouseStartPos:{x:0,y:0}, //for event handling
		_objectStartPos:{x:0,y:0}, //for event handling (moving)
		_self:undefined
	};

	
	
	var notes = { //vielleicht zu einem generellen Mediator machen, der auch Zugriff auf Canvas hat. 
		//deletedCollection: Als array mit Unterobjekten die je einen Löschvorgang beinhalten
		'setupCanvas':function(){
			this.appendTo = canvas.createCanvas();
		},
		'createNote':function(position, innerText, color){ //creates a note
			var collection = this.collection;
			var id= String.uniqueID();
			collection[id]= new Note(position, innerText, color);
			jq(this.appendTo).append(collection[id].domElement); //insert to DOM
			return id;	
		},
		'destroyNote': function(id){
			this.collection[id].domElement.remove();
			delete this.collection[id]; //delete it 
		},
		'getNotesInArea':function(area){ ///NEW
                    //Gets: an Object with the definition of an area on the canvas
                    //Returns: an array with Links to all the Notes in that area
                    //does: calculate and find all notes in a given rectangular area, and write this to "selected"
                
                    //area Object:{topLeft:number, bottomRight:number}
                    var collectionObject = this.collection; 
                    var tempTopLeftCorner;
                    var tempBottomRightCorner;
                                 
                    var tempDimensions = {height:undefined, width:undefined}; //to use in the iteration over all objects
                    var tempNoteObject;
                    
                    var notesArray=[];
                    
                    if (!area){ //First we check if the neccessary object was passed to the function
                    	console.log("Error: No areaObject passed to getNotesInArea!")
                    	return; 
                    }
                    
                    for (var tempNoteString in collectionObject){ //now we iterate over every object in the collection.
                    	tempNoteObject = collectionObject[tempNoteString];
                    	if (!(tempNoteObject instanceof Note)){ //if the object is no Note
                    		continue;  
                    	}
                    	//if the object is a Note:
                    	tempTopLeftCorner     =	tempNoteObject.getPosition();
                    	tempDimensions.height =jq(tempNoteObject.domElement).outerHeight();
                    	tempDimensions.width  =jq(tempNoteObject.domElement).outerWidth();
                    	tempBottomRightCorner = {x:tempTopLeftCorner.x+tempDimensions.width, y:tempTopLeftCorner.y+tempDimensions.height} 
                    	
                    	if((tempTopLeftCorner.x>area.topLeft.x && tempTopLeftCorner.y>area.topLeft.y) &&(tempBottomRightCorner.x<area.bottomRight.x && tempBottomRightCorner.y<area.bottomRight.y)){ //is this note inside the 
                    	
                    	//push Note to "selected" Array.  
                    	notesArray.push(tempNoteObject);
                    	
                    	}
                   
                    }
                    
                    return notesArray;
                    
                },
		'deselectNodes':function(){ ///NEW
		//deselects all notes
		//gets: nothing
		//returns:array of all notes deselected

				},

		'selected':[], //NEW
		'mouseStart':{},
		'collection':{}, //contains all notes
		'appendTo':undefined
	};
	
	var Note = new Class({ //Sollte nichts über andere Elemente wissen müssen!
	    initialize: function(position, innerText, color){
		//assign all the values
		this.domElement = jq('<div>write Text here</div>');
		this._position=position;

		this._color 		= color		||	this._defaultColor;
		this._innerText 	= innerText || 	this._defaultText;
		this._position		= position || this._defaultPosition;

		this._container = jq('#viewport')[0];
		//make it happen
		//setup the dom element

		jq(this.domElement).addClass('note');
		jq(this.domElement).css({	'top' :this._position.y+'px',
									'left':this._position.x+'px'});

		this._downHandler	= this._downHandler.bind(this);
		this._moveHandler	= this._moveHandler.bind(this);
		this._upHandler		= this._upHandler.bind(this);
		this._editText		= this._editText.bind(this);
		this._finishEditing	= this._finishEditing.bind(this);
		this._cancelEditing	= this._cancelEditing.bind(this);
		
		jq(this.domElement).on('mousedown',this._downHandler);
		jq(this.domElement).on('dblclick', this._editText);
		},
		_downHandler:function(event){
			//event.preventDefault();
			
			if(event.button !== 0){
				return
			}	

			
			
			var itemData = { //passes the currentTarget (the current Note to be moved) to the handler as well
				item: event.currentTarget,
				oldEventX:event.pageX,
				oldEventY:event.pageY,
				oldPosX:parseFloat(jq(event.currentTarget).css("left")),
				oldPosY:parseFloat(jq(event.currentTarget).css("top")),
				canvasScale:canvas._zoom //not proper, since _zoom is just supposed to be accessed form the owner itself TODO
				}

			jq(this.domElement).on('mouseup', this._upHandler);	
			jq(document).on('mouseup',this._upHandler);
                        jq(this._container).on('mousemove',itemData, this._moveHandler);
			
		},
		_moveHandler:function(event){
			event.preventDefault();
			var previousItemData = event.data;

			var offset={ //contains Difference between startposition (click) and current mouse position
				'x':event.pageX-previousItemData.oldEventX,
				'y':event.pageY-previousItemData.oldEventY
			}

			jq(previousItemData.item).css({
				'left': previousItemData.oldPosX+(offset.x*(1/previousItemData.canvasScale)),
				'top' : previousItemData.oldPosY+(offset.y*(1/previousItemData.canvasScale))
			});
		},
		_upHandler:function(event){
			jq(this._container).off('mousemove',this._moveHandler);
			jq(this.domElement).off('mouseup',this._upHandler);
		},
		_constrain:function(x,y){}, //checks on mousedown if the boundaries of canvas are exceeded.
		_editText:function(event){
			
			var jqDomElement = jq(this.domElement);
			
			jqDomElement.off('dblclick', this._editText);
			
			var form =  jq('<form>');
			var editfield = jq('<textarea>');
			var buttonOk = jq('<button type="button">o.k.</button>');
			var buttonCancel= jq('<button type="button">cancel</button>');
			var noteInnerText = jqDomElement.text();
			
			form.append(editfield);
			form.append(buttonOk);
			form.append(buttonCancel);
			
			editfield.val(noteInnerText);
		
			
			jqDomElement.append(form);
			
			buttonOk.on('click',
			    {	'editfield':editfield,
				'form':form}, 
			    this._finishEditing)
			buttonCancel.on('click',
			    {
				'form':form}, 
			    this._cancelEditing)	
		},
		_finishEditing:function(event){
			event.preventDefault();
			var jqDomElement = jq(this.domElement);
			var form = event.data.form;
			var editfield = event.data.editfield;
			
			var newText = editfield.val()
			
			jqDomElement.text(newText);
			this._innerText = newText;
			
			form.remove();
			jq(this.domElement).on('dblclick', this._editText);    
		},
		_cancelEditing:function(event){
			event.preventDefault();
			var form = event.data.form;
			form.remove();
			jq(this.domElement).on('dblclick', this._editText); 
		    
		},
		getPosition: function(){
			return this._position;
		},
		domElement:{},
		_position:{},
		_color:'',
		_innerText:"",
		_oldInnerText:"",
		_defaultColor:'yellow',
		_defaultText:"",
		_defaultPosition:{x:0,y:0},
		_mouseStartPos:{x:undefined,y:undefined},
		_oldPos:{x:undefined,y:undefined},
		_canvasScale: undefined,
		_container: undefined
	});
	
	
	
	function setup(){       
                jq('#viewport').css({
                    'position': 'absolute',
                    'overflow':'hidden',
                    'width': '400px',
                    'height': '400px',
                    'border':'black solid 1px'
                })
		
		//now do the actual setup code
		notes.setupCanvas();
		notes.createNote({x:30,y:90});
		notes.createNote({x:200,y:30});
		
		jq(document).on('mouseup', canvas.upHandler);
		jq(document).on('contextmenu', function(){return false});
		jq('#viewport').on('mouseenter',canvas.downHandler);
		jq('#viewport').on('mouseleave',canvas.upHandler);
		//jq('.item').on('mousedown', );


	}



